from pylab import subplot, subplots_adjust, axis, axes
import pylab
from matplotlib.widgets import Slider
import numpy as np

ElemBounds = np.array([0.0, 2.0, 3.0, 3.5, 4.5, 6.5])
ElemOrders = np.array([0, 1, 2, 1, 0],dtype=int)

ElemWidths = np.diff(ElemBounds)
noElem = len(ElemBounds)-1
x_low = ElemBounds[0]
x_high = ElemBounds[-1]-.001
GlobMaxOrder = max(ElemOrders)
y_low = -(GlobMaxOrder+1)
y_high = GlobMaxOrder+1
NoBasisFun = ElemOrders + 1
noDOF = sum(NoBasisFun)
gap = min(ElemWidths)*1e-5

colors = ['red', 'blue', 'black', 'green', 'magenta']
def getColor(i):
    return colors[i % len(colors)]

def FindElem(x):
    return np.array([sum(x_i>=ElemBounds)-1 for x_i in x])

def ElemCorrespondingToDOF(dof):
    TotalDOFPassed = 0
    for (ElemNo,number) in enumerate(NoBasisFun):
        TotalDOFPassed += number
        if TotalDOFPassed > dof:
            return ElemNo
    raise ValueError('DOF appears out of range')

def dof (ElemIdx, order):
    return sum(NoBasisFun[0:ElemIdx]) + order
    
def MyFun(SolVec,x):
    ElemIdx = FindElem(x)
    x_left = ElemBounds[ElemIdx]
    MaxOrders = ElemOrders[ElemIdx]
    f = np.zeros(len(x))
    for i, (point,x_left,MaxOrder,idx) in enumerate(zip(x,x_left,MaxOrders,ElemIdx)):
        for order in range(MaxOrder+1):
            f[i] += SolVec[dof(idx,order)] * ((point-x_left)/ElemWidths[idx])**order * (GlobMaxOrder+1)/(MaxOrder+1)
    return f
    
x = np.array([])
x_i = []
for i in range(noElem):
    segment = np.linspace(ElemBounds[i]+gap,ElemBounds[i+1]-gap,5)
    x_i.append(segment)
    x = np.append(x, segment)


SolVec_0 = np.array(list(range(noDOF)),dtype=float) / float(noDOF)
SolVec = SolVec_0

ax = subplot(111)
subplots_adjust(left=0.05, bottom=0.5)

PlotOfElem = []
for (ElemNo,segment) in enumerate(x_i):
    temp, = pylab.plot(segment,MyFun(SolVec_0,segment),color=getColor(ElemNo))
    PlotOfElem.append(temp)
#l, = pylab.plot(x,MyFun(SolVec_0,x))

axis([x_low, x_high, y_low, y_high])

axcolor = 'lightgoldenrodyellow'

AllSliders = []
for i in range(noDOF):
    ax = axes([0.05, 0.45-0.03*i, 0.8, 0.02], axisbg=axcolor)
    ElemNo = ElemCorrespondingToDOF(i)
    AllSliders.append(Slider(ax, 'x_'+str(i), -1.0, 1.0, valinit=SolVec_0[i],color=getColor(ElemNo)))
    
def update(_):
    for i,S in enumerate(AllSliders):
        SolVec[i] = S.val
    for (segment,plots) in zip(x_i,PlotOfElem):
        plots.set_ydata(MyFun(SolVec,segment))
    pylab.draw()

for S in AllSliders:
    S.on_changed(update)

pylab.show()
